import React, { Component } from 'react-native';

const {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  ScrollView,
} = React;

// import Router from 'react-native-simple-router';
import Toolbar from './Components/Toolbar';
import Dimensions from 'Dimensions';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

import RNIntentModule from './AndroidModules/RNIntentModule';

class Trip extends Component {
  constructor(props) {
    super(props);
    // console.warn("Trip");
    console.log(`${windowWidth}x${windowHeight}`);
    this.state = {
      topic : [
        {
          prodNo: "GRP0000004080",
          prodNm : "蒸汽火車．限量發售",
          imgUri: "http://goo.gl/OyZxAa",
          price: "11,990"
        },
        {
          prodNo: "GRP0000002259",
          prodNm : "阿里山櫻花季",
          imgUri: "http://goo.gl/pKfl53",
          price: "5,990"
        },
        {
          prodNo: "GRP0000002259",
          prodNm : "澎湖花火節．絕美登場",
          imgUri: "http://goo.gl/wCe6on",
          price: "8,990"
        },
        {
          prodNo: "GRP0000002259",
          prodNm : "金城武的天堂路+泥火山上做豆腐",
          imgUri: "http://goo.gl/Ir2HDF",
          price: "15,990"
        },
        {
          prodNo: "GRP0000002259",
          prodNm : "遠雄悅來心享受+理想自然客",
          imgUri: "http://goo.gl/8nWNh1",
          price: "15,990"
        }
      ]
    };
  }
  onPressButton(itemId) {
    if (Platform.OS === 'android'){
      RNIntentModule.goTripItemWebView(itemId);
    }
  }
  render() {
    var prod = this.state.topic.map((item, index) => {
      console.log(item, index);
      var itemStyle = () => {
        if(index === 0){
          return [styles.item, styles.itemLeft];
        }else if( index === 1){
          return [styles.item, styles.itemRight];
        }else{
          return styles.item;
        }
      };
      return (
        <TouchableOpacity key={index} style={itemStyle()} onPress={()=>this.onPressButton(item.prodNo)}>
          <Image
            style={styles.prodImg}
            source={{uri: item.imgUri}}
          />
          <View style={styles.prodTextBg}>
            <Text style={styles.prodText}>{item.prodNm}</Text>
          </View>
          <View style={styles.price}>
            <Text style={styles.priceText}>{item.price}</Text>
            <Text style={styles.priceNormal}>起</Text>
          </View>
        </TouchableOpacity>
      )
    });
    return (
    <ScrollView style={styles.container}>
      <Toolbar name="國內旅遊"/>
      <View style={styles.content}>
        {prod}
      </View>
    </ScrollView>
    );
  }
}

const gutter = 20; //商品間距
const rowNum = 1; //每列商品個數
const prodWidth = (windowWidth - (rowNum + 1) * gutter) / rowNum; //商品寬度
const prodHeight = 150; //商品高度

const col5 = (windowWidth - 3 * gutter) / 12 * 5;
const col7 = (windowWidth - 3 * gutter) / 12 * 7;

const styles = StyleSheet.create({
  container: {
    height: windowHeight,
    //flexDirection: 'column'
  },
  content: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingVertical: gutter,
    paddingHorizontal: gutter / 2,
    backgroundColor: '#ddd',
    //height: windowHeight + 200,
  },
  item: {
    width: prodWidth,
    height: prodHeight,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: gutter / 2,
    marginBottom: gutter,
  },
  itemLeft: {
    width: col5,
    height: 150,
  },
  itemRight: {
    width: col7,
    height: 150,
  },
  prodImg: {
    flex: 1,
    width: windowWidth,
    height: prodHeight,
    resizeMode: 'cover'
  },
  prodTextBg: {
    flexDirection: 'row',
    position: 'absolute',
    left: 0,
    bottom: 0,
    padding: 3,
    width: windowWidth,
    height: 50,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  price: {
    position: 'absolute',
    right: 10,
    bottom: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  priceText: {
    fontSize: 16,
    color: '#ff6600'
  },
  priceNormal: {
    fontSize: 13,
    marginLeft: 3,
    color: 'white'
  },
  prodText: {
    color: 'white',
    marginHorizontal: 5,
  }
});

export default Trip;
