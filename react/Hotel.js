import React, {
  Component,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity
}
from 'react-native';

import Toolbar from './Components/Toolbar';
import Dimensions from 'Dimensions';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

class Hotel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ad: [
        {
          adStyle: "",
          secTitle: "",
          secItems: [
            {
              title: "",
              items: []
            }
          ]
        }
      ]
    };
  }
  componentWillMount() {
    fetch('http://mweb-t01.eztravel.com.tw/api/1/ads/hotelTw')
    .then((response) => response.json())
    .then((responseText) => {
      console.log(responseText);
      this.setState({
        ad: responseText
      })
    })
    .catch((error) => {
      console.warn(error);
    });
  }
  onPressButton(itemId) {
    // if (Platform.OS === 'android'){
    //   RNIntentModule.goTripItemWebView(itemId);
    // }
  }
  render() {
    const {adStyle, secTitle, secItems} = this.state.ad[0];
    console.log(secItems[0]);
    var prods = secItems[0].items.map((itm, idx) => {
      //console.log(itm);
      return (
        <TouchableOpacity key={idx} style={styles.item}>
          <Image
            style={styles.prodImg}
            source={{uri: itm.產品圖片}}
          />
          <Text style={styles.prodText} numberOfLines={1}>{itm.產品標題}</Text>
          <View style={styles.prodTextBg}>
              <Text style={styles.prodTextSub}>{itm.產品副標}</Text>

                <Text style={styles.priceText}>{itm.產品價格}</Text>
                <Text style={styles.priceNormal}>起</Text>

          </View>

        </TouchableOpacity>
      );
    });
    return (
      <View style={styles.container}>
        <View style={styles.title}>
          <View style={styles.titleBlock}>
            <View style={styles.titleGreenBar}></View>
            <Text style={styles.titleText}>{secTitle}</Text>
          </View>
          <TouchableOpacity style={styles.titleBlock}>
            <Text style={styles.moreText}>更多{secTitle}</Text>
            <Image style={styles.moreIcon} source={require('./common/img/ic_more_right_green.png')} />
          </TouchableOpacity>
        </View>
        <View style={styles.content}>
          {prods}
        </View>
      </View>
    );
  }
}
const ci_Green = '#81c31e';

const gutter = 10; //商品間距
const rowNum = 2; //每列商品個數
const prodWidth = (windowWidth - (rowNum + 1) * gutter) / rowNum; //商品寬度
const prodHeight = 150; //商品高度

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',

  },
  title: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'white'
  },
  titleBlock: {
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'red'
  },
  titleGreenBar: {
    width: 5,
    height: 20,
    marginRight: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: ci_Green
  },
  titleText: {
    fontSize: 16
  },
  moreText: {
    color: '#666666',
  },
  moreIcon: {
    width: 10,
    height: 16,
    marginRight: gutter,
    marginLeft: 5
  },
  content: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingVertical: gutter,
    paddingHorizontal: gutter / 2,
    backgroundColor: 'white'
  },
  item: {
    width: prodWidth,
    height: prodHeight,
    // alignItems: 'center',
    // justifyContent: 'center',
    marginHorizontal: gutter / 2,
    marginBottom: gutter,
    flexDirection: 'column',
  },
  prodImg: {
    flex: 1,
    width: prodWidth,
    height: prodHeight,
    resizeMode: 'cover'
  },
  prodText: {
    marginTop: 3
  },
  prodTextBg: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  price: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  priceText: {
    fontSize: 16,
    color: '#ff6600',
  },
  priceNormal: {
    fontSize: 13,
    marginLeft: 3,
  },
  prodTextSub: {
    flex: 1,
    color: '#999',
    fontSize: 13
  }
});

export default Hotel;
