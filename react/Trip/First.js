import React, {
  Component,
  StyleSheet,
  View,
  PropTypes,
  Text,
}
from 'react-native';

const propTypes = {
  toRoute: PropTypes.func.isRequired,
};


class First extends Component {
  constructor(props) {
    super(props);
    console.warn('First');
  }
  render() {
    return (
      <View style={styles.container}>
      	<Text>First</Text>
        <Text>First</Text>

        <Text>First</Text><Text>First</Text>
        <Text>First</Text><Text>First</Text>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'blue'
  }
});


First.propTypes = propTypes;
export default First;
