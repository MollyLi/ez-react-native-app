import React, { Component } from 'react-native';

const {
  StyleSheet,
  View,
  Text,
} = React;

class Second extends Component {
  constructor(props) {
    super(props);
    console.warn('Second');
  }
  render() {
    return (
      <View style={styles.container}>
      	<Text>2</Text>
      </View>
    );
  }
}

var styles = StyleSheet.create({

});


export default Second;
