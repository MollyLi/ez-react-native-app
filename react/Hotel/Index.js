import React, { Component } from 'react-native';

const {
  StyleSheet,
  View,
  Text
} = React;

class Index extends Component {
  render() {
    return (
      <View style={styles.container}>
      	<Text>Hotel Index</Text>
      </View>
    );
  }
}

var styles = StyleSheet.create({
	container: {
		backgroundColor: 'blue'
	}
});


export default Index;
