'use strict'
import React, {
  Component,
  Image,
  View,
  PropTypes,
  StyleSheet
} from 'react-native'

export default class BackButton extends Component {
  constructor(props) {
    super(props);
    console.warn('ic_action_back');
  }

  render() {
    return (
      <Image source={{uri: 'https://facebook.github.io/react/img/logo_og.png'}} style={styles.backButton} />
    );
  }
}

BackButton.propTypes = {
};

const styles = StyleSheet.create({
  backButton: {
    width: 40,
    height: 30,
    marginLeft: 10,
    marginTop: 3,
    marginRight: 10
  }
});
