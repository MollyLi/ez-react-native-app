import React, { Component } from 'react-native';

const {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  NativeModules,
} = React;

import RNIntentModule from '../AndroidModules/RNIntentModule';

class Toolbar extends Component {
  render() {
    return (
      <View style={styles.toolbar}>
      	<TouchableOpacity style={styles.backButton} onPress={this.onPressButton}>
        	<Image
        	  style={styles.backImage}
        	  source={require('./img/ic_action_back.png')} />
        	<Text style={styles.title}>{this.props.name}</Text>
        </TouchableOpacity>
      </View>
    );
  }
  onPressButton() {
  	RNIntentModule.callAndroidView("send value"); //呼叫android 首頁
  }
}

var styles = StyleSheet.create({
	toolbar:{
		flex: 1,
    backgroundColor:'white',
    borderBottomWidth: 0.5,
    borderBottomColor: '#BFBFBF',
    height: 50,
    flexDirection:'row',
  },
  backButton: {
  	width: 150,
  	flexDirection:'row',
    justifyContent: 'center', 
		alignItems: 'center',
		//backgroundColor: 'red'
  },
  backImage:{
    width: 36,
    height: 36
  },
  title:{
    color:'#666',
    fontSize: 19,
    marginTop: -2,
    flex:1
  },
});


export default Toolbar;
