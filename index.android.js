import React, { Component } from 'react-native';

const {
  StyleSheet,
  View,
  AppRegistry,
  NativeModules,
} = React;

//import Router from 'react-native-simple-router';
//import BackButton from './react/components/BackButton';
import Trip from './react/Trip';
import Hotel from './react/Hotel';
import RNIntentModule from './react/AndroidModules/RNIntentModule';
import codePush from "react-native-code-push";

class ezReactNativeApp extends Component {
  constructor(props) {
    super(props);
    //RNIntentModule.LINE //從 android 傳送的資料
    // console.warn(RNIntentModule.LINE);

    //RNIntentModule.callAndroidView("send value"); //呼叫android 首頁
  }

  componentDidMount() {
    // codePush.checkForUpdate()
    //   .then((update) => {
    //     if (!update) {
    //       console.log("The app is up to date!");
    //     } else {
    //       console.log("An update is available! Should we download it?");
    //     }
    //   });
    // codePush.sync();
  }

  render() {
    //const line = RNIntentModule.LINE; //從 android 傳送的資料
    const line = 'hotelTw';
    console.log(line);
      return (
        //false
        <View>
          {
            line === 'react' && <Trip />
          }
          {
            line === 'hotelTw' && <Hotel line={line}/>
          }
        </View>
      );
    }
}

// const styles = StyleSheet.create({
//    container: {
//   }
// });

AppRegistry.registerComponent('ezApp', () => ezReactNativeApp);
