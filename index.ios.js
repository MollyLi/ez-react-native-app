/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View
} from 'react-native';

import Hotel from './react/Hotel';

class ezReactNativeApp extends Component {
  render() {
    return (
      <View>
        <Hotel />
      </View>
    );
  }
}

const styles = StyleSheet.create({

});

AppRegistry.registerComponent('ezApp', () => ezReactNativeApp);
